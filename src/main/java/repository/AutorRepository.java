package repository;

import org.springframework.data.jpa.repository.JpaRepository;

import models.Autor;


public interface AutorRepository extends JpaRepository<Autor, Long>{

}
