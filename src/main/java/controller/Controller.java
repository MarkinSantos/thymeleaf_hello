package controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@org.springframework.stereotype.Controller
public class Controller {
	
	@GetMapping("/lista")
	public ModelAndView getList() {
		
		ModelAndView mv = new ModelAndView("index");
		return mv;
		
	}

}
